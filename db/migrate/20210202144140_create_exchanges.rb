# frozen_string_literal: true

ROM::SQL.migration do
  change do
      create_table :exchanges do
        primary_key :id
        column :amount, Float, null: false
        column :from, String, null: false
        column :to, String, null: false
        column :converted_amount, Float, null: false
        column :created_at, DateTime
        column :updated_at, DateTime
    end
  end
end
