# For the documentation check http://sinatrarb.com/intro.html
require 'money/bank/currencylayer_bank'
require_relative '../../config/db'

$mclb = Money::Bank::CurrencylayerBank.new
$mclb.access_key = ENV['CURRENCYLAYER_API_KEY']
Money.default_bank = $mclb

class ApplicationController < Sinatra::Base
	# This configuration part will inform the app where to search for the views and from where it will serve the static files
  	configure do
    	set :views, "app/views"
    	set :public_dir, "public"
  	end

  	get '/' do
		   erb :index
	end
	  
	post '/convert' do
		# Get all the parameters
		@amount = params[:amount]
		@from = params[:from]
		@to = params[:to]

		# Modify params for currencylayer API
		amount_in_cents = @amount.to_f * 100

		# If 'From' and 'To' rates are same, render the template with error message
		@converted_amount = @from != @to ? Money.new(amount_in_cents, @from).exchange_to(@to).to_f : nil
		if @converted_amount
			exchanges = CONTAINER.relations[:exchanges]
			exchanges.command(:create).call(amount: @amount, from: @from, to: @to, converted_amount: @converted_amount)
		end
		erb :result
	end

	get '/history' do
		exchanges = CONTAINER.relations[:exchanges]
		@exchanges_history = exchanges.order{ created_at.desc }.limit(5).as_hash
		erb :history
	end

end