class CreateExchange < ROM::Commands::Create[:sql]
    relation :exchanges
    register_as :create
    result :one

    use :timestamps
    timestamp :created_at, :updated_at
end