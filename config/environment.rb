# Setting the app envirement
ENV['SINATRA_ENV'] ||= "development"
ENV['RACK_ENV'] ||= "development"
# Add the needed requirement to boot the app
require 'bundler/setup'
require 'rubygems'
require 'dotenv'
Bundler.require(:default, ENV['SINATRA_ENV'])
Dotenv.load(".env", ".env.#{ENV["SINATRA_ENV"]}")

# Load the DB configured settings
require_relative 'db'

# Loading all the files in app folder
require_all 'app'