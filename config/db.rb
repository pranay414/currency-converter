require 'rom'
require 'rom-sql'
# Setting up database connection
connection = Sequel.connect(ENV['DATABASE_URL'])
configuration = ROM::Configuration.new(:sql, connection)

# Auto-register relations, this is a part of ROM framework.
configuration.auto_registration('./app/lib', namespace: false)

CONTAINER = ROM.container(configuration)